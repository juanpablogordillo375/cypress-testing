describe("API test", () => {
    it("Objects with Authentication and Authorization category", () => {
        // Make the API call
        cy.request("GET", "https://api.publicapis.org/entries")
            .then((response) => {
                // Make an assertion over the status code
                expect(response.status).to.equal(200);
                const body = response.body;
                
                // Filter the objects using the given category
                const objects = body.entries.filter((entry) => {
                    return entry.Category.includes(Cypress.env("API_CATEGORY"))
                });

                // Make an assertion over the amount of objects w/ that category
                expect(objects.length).to.be.greaterThan(0);

                // Count occurrences of uniques API
                const apiMap = {};
                objects.forEach((entry) => {
                    cy.log(`Found Object: ${JSON.stringify(entry)}`);
                    const api = entry.API;
                    apiMap[api] = (apiMap[api] || 0) + 1;
                });

                // Each unique API should contain at least 1
                for(const api in apiMap){
                    const count = apiMap[api];
                    // cy.log(`Amount of objects with API ${api}: ${count}`);
                    expect(count, `${api}`).to.be.greaterThan(0);
                }

                // Logs the amount of matching objects w/ given category
                cy.log(`Amount of Objects that matches category: ${objects.length}`);
        });    
    });
});