const uiTest = require('../constants.js');

describe("UI test", () => {
    it("Should change the sorting name", function() {
        // Perform login using custom command
        cy.login();

        // Make sure that the dropdown exists and the default value is A -> Z
        cy.get(uiTest.dropdown)
            .should("be.visible")
            .should("exist")
            .invoke("val")
            .then((selected) => {
                expect(selected).to.equal("az");
            });
        
        // Grab the name of the first item and store its value
        cy.get(uiTest.firstItem)
            .should("be.visible")
            .should("exist")
            .invoke("text")
            .as("initialValue");
        
        // Change sorting order to Z -> A
        cy.get(uiTest.dropdown).select("za");
        
        // Grab the name of the first item and compare them with the previous value
        cy.get(uiTest.firstItem)
            .invoke("text")
            .then((updatedValue) => {
                cy.get("@initialValue").should("not.equal", updatedValue);
            });
        
        // Verify that the dropdown value has changed to Z -> A
        cy.get(uiTest.dropdown)
            .invoke("val")
            .then((selected) => {
                expect(selected).to.equal("za");
            });
    });
});
