const uiTest = {
  dropdown: '[data-test="product_sort_container"]',
  firstItem: '.inventory_item:first-of-type .inventory_item_name',
};

module.exports = uiTest;