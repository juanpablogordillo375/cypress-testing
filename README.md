## Prerequisites
    - Node.js installed

## Installation
    1. Clone the repository:
        - git clone <repository-url>
    2. Open a terminal and navigate to the project folder:
        - cd cypress-testing
    3. Install dependencies:
        - npm install
## How to run the tests
    1. API Test:
        - npx cypress run --spec cypress/src/api/apiTest.cy.js
    2. UI Test:
        - npx cypress run --spec cypress/src/e2e/uiTest.cy.js
    3. Run all:
        - npx cypress run