const { defineConfig } = require("cypress");

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    watchForFileChanges : true,
    specPattern : "cypress/src/**/*.cy.{js,jsx,ts,tsx}",
    screenshotOnRunFailure : false,
    video: false,
    baseUrl: "https://www.saucedemo.com"
  },
});
